package com.tbuttler.collection;

public class CalculatorTree {

	private Entry root;

	public CalculatorTree() {
		root = new Entry();
	}

	public void startWith(Operation left, Operation value, Operation righOperation) {
		root.value = value;
		root.left = Entry.createLeaf(left, root);
		root.right = Entry.createLeaf(righOperation, root);
	}
	
	public void startWith(Value value, Operation op) {
		Entry leaf = Entry.createLeaf(value, null);
		root = Entry.createRoot(op);
		root.left = leaf;
		leaf.parent = root;
	}
	
	public void append(Value value, Operation op) {
		if (root.value == null) {//first entry
			startWith(value, op);
		} else {
			append(op, value);
		}
	}

	public void closeWith(Value value) {
		root.right = Entry.createLeaf(value, root);
	}

	public void append(Operation op, Value value) {
		root = Entry.combineTrees(op, root, Entry.createLeaf(value, null));
	}

	public void append(Operation op, CalculatorTree rightExpression) {
		Entry right = rightExpression.root;
		root = Entry.combineTrees(op, root, right);
	}

	public void inBrackets(CalculatorTree inside) {
		root.right = inside.root;
		inside.root.parent = root;
	}

	public double calc() {
		return root.eval();
	}

	private static class Entry {

		private Entry left;
		private Entry right;
		private Entry parent;
		private Operation value;
		
		static Entry createRoot(Operation op) {
			Entry root = new Entry();
			root.value = op;
			return root;
		}

		static Entry createLeaf(Operation value, Entry parent) {
			Entry leaf = new Entry();
			leaf.value = value;
			leaf.parent = parent;
			return leaf;
		}
		
		static Entry combineTrees(Operation value, Entry left, Entry right) {
			Entry root = new Entry();
			root.left = left;
			root.right = right;
			root.value = value;
			left.parent = root;
			right.parent = root;
			return root;
		}

		public double eval() {
			double leftValue = 0;
			double rightValue = 0;
			if (left != null)
				leftValue = left.eval();
			if (right != null)
				rightValue = right.eval();
			return value.calc(leftValue, rightValue);
		}

	}

	static class Operation {

		static final String PLUS = "PLUS";
		static final String MINUS = "MINUS";
		static final String MULT = "MULT";
		static final String DIVISON = "DIVISON";

		private String op;

		Operation(String type) {
			this.op = type;
		}

		double calc() {
			return 0;
		}

		double calc(double a, double b) {
			System.out.println(op);
			switch (op) {
			case PLUS:
				return a + b;
			case MINUS:
				return a - b;
			case MULT:
				return a * b;
			case DIVISON:
				return a / b;
			default:
				return calc();
			}
		}

	}

	static class Value extends Operation {

		static final String IDENTITY = "IDENTITY";

		private double v;

		public Value(double value) {
			super(IDENTITY);
			v = value;
		}

		@Override
		double calc() {
			return v;
		}
	}


}
