package com.tbuttler.collection.queues;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.StdRandom;

/**
 * <p>
 * A randomized queue stores items in a queue. Unlike other queues, a randomized
 * {@link RandomizedQueue#dequeue()} returns a random item from the queue and
 * removes it. {@link RandomizedQueue#sample()} returns a random item without
 * removing it from the queue.
 * </p>
 *
 * <p>
 * The queue provides an {@link Iterator} that returns the items in the queue in
 * random order. Two iterators return the items in two independent orders.
 * </p>
 *
 * <p>
 * <b>Performance and Space.</b> All operations (with the exception of creating
 * an iterator) are, on average, performed in constant time. The worst case for
 * {@link RandomizedQueue#enqueue(Object)} and {@link RandomizedQueue#dequeue()}
 * is linear time. The space requirement for n items in the queue is in O(n).
 * Creating an iterator takes linear time, and requires additional linear space.
 * </p>
 *
 *
 *
 * @author Tanja
 *
 * @param <ItemT>
 *            the type stored in the queue
 */
public class RandomizedQueue<ItemT> implements Iterable<ItemT> {

    /**
     * Array storing the items. The array is filled between 25% (right before
     * shrinking it) and 100% (right before expanding it).
     */
    private Object[] items;

    /**
     * The number of items stored in this collection. The size differs from the
     * {@linkplain items.lenght}, as the array can contain unoccupied space.
     */
    private int size;

    /**
     * A threshold value determining when an array should shrink. More
     * precisely, the array should shrink if
     * <code>size * threshold < items.length</code>
     * 
     * @see RandomizedQueue#needsShrinking()
     */
    private static final int THRESHOLD = 4;

    /**
     * Construct an empty randomized queue.
     */
    public RandomizedQueue() {
        size = 0;
    }

    /**
     * Is the queue empty?
     * 
     * @return true if the queue is empty
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Return the number of items in the queue.
     * 
     * @return the number of items in the queue
     */
    public int size() {
        return size;
    }

    /**
     * Add the item to the end of the queue.
     * 
     * @param item
     *            queuing up
     */
    public void enqueue(ItemT item) {
        validate(item);
        if (items == null) {
            items = new Object[2];
        } else if (size() == items.length) {
            expandArray();
        }
        items[size] = item;
        size++;
    }

    /**
     * Checks the validity of the item.
     * 
     * @implSpec if items fail the check, throw an exception rather than failing
     *           gracefully
     * 
     * @param item
     *            to be checked
     * @throws NullPointerException
     *             if the specified item is null
     */
    private void validate(ItemT item) {
        if (item == null) {
            throw new NullPointerException(
                    "This queue does not accept null items.");
        }
    }

    /**
     * Expands the array doubling its length. Should be used when the array is
     * full.
     */
    private void expandArray() {
        items = copyItemsToNewArray(size() * 2);
    }

    /**
     * Helper method to create an array of newSize, and copy the content of the
     * old array into the beginning of the new array. If newSize is smaller than
     * 2, a new array of size 2 is created instead.
     * 
     * @param newSize
     *            the expected new size of the array
     * @return array with the same content but in the newSize
     */
    private Object[] copyItemsToNewArray(int newSize) {
        Object[] newArray = new Object[Math.max(newSize, 2)];
        for (int i = 0; i < size(); i++) {
            newArray[i] = items[i];
        }
        return newArray;
    }

    /**
     * Shrinks the array to half its length. Should be used when the array is
     * only a quarter full to leave room for expansions.
     */
    private void shrinkArray() {
        items = copyItemsToNewArray(items.length / 2);
    }

    /**
     * Remove and return a random item.
     * 
     * @return a random item
     * @throws NoSuchElementException
     *             if the queue is empty
     */
    public ItemT dequeue() {
        validateAccess();
        int position = StdRandom.uniform(size());
        @SuppressWarnings("unchecked") // arrays do not support Generics in
                                       // Java, so this is part of the
                                       // workaround
        ItemT result = (ItemT) items[position];
        int last = size() - 1;
        items[position] = items[last];
        items[last] = null;
        size--;
        if (!isEmpty() && needsShrinking()) {
            shrinkArray();
        }
        return result;
    }

    /**
     * Tests whether the array has too many empty slots. The current threshold
     * is for the array to be filled to 25%. Below that threshold, the array
     * needs shrinking.
     * 
     * @return true if the array has too many empty slots, and therefore needs
     *         shrinking
     */
    private boolean needsShrinking() {
        return (size() * THRESHOLD) < items.length;
    }

    /**
     * Checks whether it is allowed to access items in the queue. This
     * implementation throws an Exception if the queue is empty.
     * 
     * @implSpec if items fail the check, throw an exception rather than failing
     *           gracefully
     * 
     * @throws NoSuchElementException
     *             if the queue is empty
     */
    private void validateAccess() {
        if (isEmpty()) {
            throw new NoSuchElementException(
                    "You cannot access an item in an empty queue");
        }
    }

    /**
     * Return (but do not remove) a random item.
     * 
     * @return a random item from the queue
     * @throws NoSuchElementException
     *             if the queue is empty
     */
    @SuppressWarnings("unchecked") // arrays do not support Generics in Java, so
                                   // this is part of the workaround
    public ItemT sample() {
        validateAccess();
        return (ItemT) items[StdRandom.uniform(size())];
    }

    /**
     * Return an iterator over items for the foreach loop. Items are returned in
     * random order; the retrieval order of two iterators is independent.
     * 
     * @return an iterator over the queue
     */
    public Iterator<ItemT> iterator() {
        return new RandomIter<ItemT>(items, size());
    } // return an independent iterator over items in random order

    /**
     * <p>
     * An {@link Iterator} that returns the items in the queue in random order.
     * Two iterators return the items in two independent orders.
     * </p>
     * 
     * <p>
     * Creating an iterator takes linear time, and requires additional linear
     * space. Other operations of the iterator are performed in constant time
     * (worst case).
     * </p>
     * 
     * @author Tanja
     *
     * @param <ItemT>
     *            type stored in the collection
     */
    private static class RandomIter<ItemT> implements Iterator<ItemT> {

        /**
         * The order in which the items are retrieved. Each item in this array
         * represents a position of an item in the content array. The positions
         * are in random order.
         */
        private int[] order;

        /**
         * Reference to the actual content of the array.
         */
        private Object[] content;

        /**
         * Current position of the iterator in the order array.
         */
        private int position;

        /**
         * Creates a new, non-modifying iterator for a RandomizedQueue.
         * 
         * @param items
         *            reference to the content of the array.
         * @param size
         *            number of items in the queue
         */
        RandomIter(Object[] items, int size) {
            initializeRandomOrder(size);
            position = 0;
            content = items;
        }

        /**
         * Initializes the order array with a new, randomized order for [0,
         * size).
         * 
         * @param size
         *            number of items in the content array
         */
        private void initializeRandomOrder(int size) {
            order = new int[size];
            for (int i = 0; i < order.length; i++) {
                order[i] = i;
            }
            StdRandom.shuffle(order);
        }

        @Override
        public boolean hasNext() {
            return position < order.length;
        }

        @Override
        public ItemT next() {
            validateAccess();
            int nextRandom = order[position];
            position++;
            @SuppressWarnings("unchecked") // arrays do not support Generics in
                                           // Java, so this is part of the
                                           // workaround
            ItemT result = (ItemT) content[nextRandom];
            return result;
        }

        /**
         * Checks whether it is allowed to access the next item in queue. This
         * implementation throws an Exception if the iterator has reached the
         * end of the queue.
         * 
         * @implSpec throws an exception rather than failing gracefully if items
         *           fail the check
         * 
         * @throws NoSuchElementException
         *             if the iterator has reached the end of the queue
         */
        private void validateAccess() {
            if (position == order.length) {
                throw new NoSuchElementException(
                        "You cannot access an item in an empty queue");
            }
        }

    }

}
