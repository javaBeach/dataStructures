package com.tbuttler.collection.queues;

import java.util.Iterator;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Permutation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int k = Integer.valueOf(args[0]);
		RandomizedQueue<String> input = new RandomizedQueue<>();
		while (!StdIn.isEmpty()) {
			input.enqueue(StdIn.readString());
		}
		Iterator<String> iter = input.iterator();
		for (int i = 0; i < k; i++) {
			StdOut.printf(iter.next());
		}
	}

}
