package com.tbuttler.collection.queues;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Implementation of an {@link IDeque} that performs all operations in constant time (worst case).
 * 
 * This implementation allows objects to be a target for the foreach-loops.
 * 
 * 
 * @author Tanja
 *
 * @param <Item> the type of element stored in this collection
 * @implNote The deque has been implemented using a double-linked list in order to ensure constant time.
 */
public class Deque<Item> implements Iterable<Item> {

	private Node<Item> first;

	private Node<Item> last;

	private int size;

	/**
	 * Creates an empty deque.
	 */
	public Deque() {
		first = null;
		last = null;
		size = 0;
	}

	@Override
	public Iterator<Item> iterator() {
		return new DequeIterator<>(first);
	}

	//@Override
	public boolean isEmpty() {
		return size == 0;
	}

	//@Override
	public int size() {
		return size;
	}

	//@Override
	public void addFirst(Item item) {
		validate(item);
		Node<Item> node = new Node<>(item);
		if (size() > 0) {// prepend
			node.setNext(first);
			first.setPrevious(node);
		} else {// first node in the deque
			last = node;
		}
		first = node;
		size++;
	}

	/**
	 * Checks the validity of the item.
	 * 
	 * @implSpec if items fail the check, throw an exception 
	 * 
	 * @param item to be checked
	 * @throws NullPointerException  if the specified item is null
	 */
	private void validate(Item item) {
		if (item == null)
			throw new NullPointerException("This deque does not accept null items.");
	}

	//@Override
	public void addLast(Item item) {
		validate(item);
		Node<Item> node = new Node<>(item);
		if (size() > 0) {// append
			node.setPrevious(last);
			last.setNext(node);
		} else {// first node in the deque
			first = node;
		}
		last = node;
		size++;
	}

	//@Override
	public Item removeFirst() {
		Node<Item> removed = first;
		if (size() > 1) {
			first = removed.getNext();
			first.setPrevious(null);
		} else {
			removeLastNode();
		}
		size--;
		return removed.getItem();
	}

	//@Override
	public Item removeLast() {
		Node<Item> removed = last;
		if (size() > 1) {
			last = removed.getPrevious();
			last.setNext(null);
		} else {
			removeLastNode();
		}
		size--;
		return removed.getItem();
	}

	/**
	 * Resets the linked list if it only contains one node. Use on deques that still contain one node.
	 * 
	 * @implSpec 
	 * Reset the pointers to the first and last node to their initial state.
	 * The size is not changed.
	 * @throws NoSuchElementException if the method is called on an empty deque
	 */
	private void removeLastNode() {
		if (size() == 1) {
			first = null;
			last = null;
		} else if(size < 1) {
			throw new NoSuchElementException("Attempt to remove an item from an empty deque");
		}
	}

	/**
	 * Iterates through the Deque from front to end. Uses the default behavior for {@link Iterator#remove}.
	 * 
	 * @author Tanja
	 *
	 * @param <Item> the type of element stored in the collection
	 */
	private static class DequeIterator<Item> implements Iterator<Item> {

		/*
		 * Current position in the deque.
		 */
		private Node<Item> position;

		/**
		 * Initializes the iterator with the first node.
		 * From  
		 * @param start 
		 */
		public DequeIterator(Node<Item> start) {
			position = start;
		}

		@Override
		public boolean hasNext() {
			return position != null;
		}

		@Override
		public Item next() {
			if (hasNext()) {
				Node<Item> result = position;
				position = result.getNext();
				return result.getItem();
			} else {
				throw new NoSuchElementException("The iterator has no more elements in the deque");
			}
		}
//		
//		public void remove() {
//			throw new UnsupportedOperationException("This iterator does not support removing items from the deque.");
//		}
	}
	
	/**
	 * A single node in a double linked list. The node stores a single object (the item), and
	 * has a pointer to its predecessor ({@link Node#getPrevious}) and its successor ({@link Node#getNext}) in the list.
	 * 
	 * @author Tanja
	 *
	 * @param <Item> the type of element stored in this node
	 */
	private static class Node<Item> {

		private Item item;
		private Node<Item> next;
		private Node<Item> previous;

		/**
		 * Creates an unconnected node for storing the item.
		 * @param item to be stored in the node
		 */
		Node(Item item) {
			setItem(item);
		}

		/**
		 * Retrieves the stored item.
		 * @return the stored item
		 */
		Item getItem() {
			return item;
		}

		/**
		 * Changes the item stored in this node.
		 * @param item the new item
		 */
		void setItem(Item item) {
			this.item = item;
		}

		/**
		 * Retrieves the next node in the list (main direction: from front to back)
		 * @return the node following this node, or <code>null</code> if no such node exists
		 */
		Node<Item> getNext() {
			return next;
		}

		/**
		 * Sets a successor for this node.
		 * 
		 * @param next the successor
		 */
		void setNext(Node<Item> next) {
			this.next = next;
		}

		/**
		 * Retrieves the previous node in the list (allowing a client to move in the list from
		 * back to front).
		 * 
		 * @return the predecessor of this node, or <code>null</code> if no such node exists
		 */
		Node<Item> getPrevious() {
			return previous;
		}

		/**
		 * Sets the predecessor of this node.
		 * 
		 * @param previous the predecessor
		 */
		void setPrevious(Node<Item> previous) {
			this.previous = previous;
		}

	}

}
