package com.tbuttler.collection;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Wraps a collection that alternately adds objects of type T to the left and right end of a collection.
 * 
 * The method {@link toString} prints out the actual ascending order of the list, while an iterator is provided that
 * can access the elements in the order in which they were inserted.
 * @author Tanja
 *
 * @param <T>
 */
public class LeftRightList<T> {
	
	private LinkedList<T> objects;
	private boolean continueLeft;
	
	public LeftRightList() {
		continueLeft = true;
		objects = new LinkedList<T>();
	}
	
	public String toString() {
		//The string representation orders the elements in their internal order
		return objects.toString();
	}
	
	public int size() {
		return objects.size();
	}
	
	public boolean isEmpty() {
		return objects.isEmpty();
	}
	
	public void add(T o) {
		//iterate between inserting objects left and right
		if(continueLeft) {
			objects.addFirst(o);
			continueLeft = false;
		} else {
			objects.addLast(o);
			continueLeft = true;
		}
	}	

	public int hashCode() {// keep the contract
		return objects.hashCode();
	}
	
	public Iterator<T> insertionOrderIterator() {
		return new LRIterator();
	}
	
	
	
	public class LRIterator implements Iterator<T> {		
		
		private boolean continueLeft;
		private int numRetrievals;
		private int cursor;
		
		public LRIterator() {
			continueLeft = true;
			numRetrievals = 0;
			cursor = (size() -1)/2;
		}

		@Override
		public boolean hasNext() {
			return numRetrievals < size();
		}

		@Override
		public T next() {
			// go through the elements in the order in which they were inserted
			T result = null;
			if(continueLeft) {
				result = objects.get(cursor);
				continueLeft = false;
				numRetrievals++;
			} else {
				cursor = cursor + numRetrievals; // move the cursor past the middle point
				result = objects.get(cursor);
				numRetrievals++;
				cursor = cursor - numRetrievals; //flip it back to before the next element on the left side
				continueLeft = true;
			}
			return result;
		}
		
	}
	
	
	
}