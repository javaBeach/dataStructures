package com.tbuttler.unionFind;

public abstract class AbstractQuickUnion implements UnionFind {

	/**
	 * Array with values pointing (== containing the index of) to the parent node, or, if it is a root node, to itself.
	 * The pointers create a tree structure.
	 */
	private int[] parentNodes;

	public AbstractQuickUnion(int size) {
		setParentNodes(new int[size]);
		for (int node = 0; node < getParentNodes().length; node++) {
			//assume that each node is a root, i.e., it points to itself
			getParentNodes()[node] = node;
		}
	}

	/**
	 * Check if the two nodes are connected by checking whether they are connected to the same root node.
	 * 
	 * Worst case: O(n)
	 */
	@Override
	public boolean connected(int a, int b) {
		int rootA = findRoot(a);
		int rootB = findRoot(b);
		return rootA == rootB;
	}

	protected void changeParentNode(int of, int newParent) {
		getParentNodes()[of] = newParent;
	}

	/**
	 * Identify the root of a node. 
	 * 
	 * Worst case: O(n)
	 * 
	 * @param node
	 * @return
	 */
	protected int findRoot(int node) {
		while(!isRoot(node)) {//move up the tree
			node = getParentNodes()[node];
		}
		return node;
	}

	/**
	 * Checks whether the node is actually a root node.
	 * @param node
	 * @return
	 */
	protected boolean isRoot(int node) {
		return getParentNodes()[node] == node;
	}

	protected int[] getParentNodes() {
		return parentNodes;
	}

	protected void setParentNodes(int[] parentNodes) {
		this.parentNodes = parentNodes;
	}

}