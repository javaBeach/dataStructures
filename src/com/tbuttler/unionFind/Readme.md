# UnionFind

UnionFind is a datastructure represinting sets, that allows merging two sets by 
connecting two elements.

This package contains various implementations of this datastructure.

<!--

|Implementation | isConnected | union(a, b) | structure and algorithm |
|---------------|-------------|-------------|-------------------------|

-->