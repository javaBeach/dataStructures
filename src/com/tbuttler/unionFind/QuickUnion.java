package com.tbuttler.unionFind;

/**
 * Uses an (unbalanced) forest to represent the sets.
 * 
 * @author Tanja
 *
 */
public class QuickUnion extends AbstractQuickUnion implements UnionFind {
	
	public QuickUnion(int size) {
		super(size);
	}


	/**
	 * Create a union of two sets by setting the root of one set as the parent of the other set.
	 * 
	 * Worst case: O(n)
	 */
	@Override
	public void union(int a, int b) {
		int rootA = findRoot(a);
		int rootB = findRoot(b);
		if(rootA != rootB) {
			changeParentNode(rootA, rootB);
		}
	}

}
