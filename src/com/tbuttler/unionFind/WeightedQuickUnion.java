package com.tbuttler.unionFind;

/**
 * Implementation of union find using an array-based weighted tree structure.
 * 
 * O(log n) time for querying whether two elements are in the same  
 * 
 * @author Tanja
 *
 */
public class WeightedQuickUnion extends QuickUnion implements UnionFind {
	
	private int[] treeDepth;

	public WeightedQuickUnion(int size) {
		super(size);
		treeDepth = new int[size];
		for (int node = 0; node < treeDepth.length; node++) {
			treeDepth[node] = 1;
		}
	}
	
	/**
	 * Attaches the smaller tree to the larger tree to keep the depth small.
	 */
	@Override
	public void union(int a, int b) {
		int rootA = findRoot(a);
		int rootB = findRoot(b);
		if(rootA == rootB)
			return;
		//make the larger tree the new parent
		if(treeDepth[rootA] < treeDepth[rootB]) {
			changeParentNode(rootA, rootB); 
			// b is really smaller than a, so the tree size does not change
		} else if (treeDepth[rootA] == treeDepth[rootB]){
			changeParentNode(rootA, rootB);
			treeDepth[rootB] = treeDepth[rootB] + 1;			
		} else {
			changeParentNode(rootB, rootA);
		}
	}
	
	

}
