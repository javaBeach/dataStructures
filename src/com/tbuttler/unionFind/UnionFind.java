package com.tbuttler.unionFind;

/**
 * Models a set of n elements. Supports merging the sets associated with two elements by
 * connecting two elements, and querying whether two elements are part of the same
 * set.
 * 
 * @author Tanja
 *
 */
public interface UnionFind {
	
	/**
	 * Returns true if nodes with the indices a and b are in the same connected sub-graph.
	 * 
	 * @param a
	 * @param b
	 */
	public boolean connected(int a, int b);
	
	/**
	 * Creates a union of the sub-graphs containing a and b.
	 * 
	 * @param a
	 * @param b
	 */
	public void union(int a, int b);

}
