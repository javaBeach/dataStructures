package com.tbuttler.unionFind;

public class QuickUnionWPathCompression extends AbstractQuickUnion implements UnionFind {
	
	//for the element at position, treeSize stores the number of nodes in the subtree
	private int[] treeSize;
	
	public QuickUnionWPathCompression(int size) {
		super(size);
		treeSize = new int[size];
		for (int i = 0; i < treeSize.length; i++) {
			treeSize[i] = 0;
		}
	}

	@Override
	public void union(int a, int b) {
		int rootA = findRoot(a);
		int rootB = findRoot(b);
		if(rootA == rootB)
			return;
		//make the larger tree the new parent
		if(treeSize[rootA] <= treeSize[rootB]) {
			changeParentNode(rootA, rootB);
			treeSize[rootB] = treeSize[rootB]//current tree with root B 
							+ treeSize[rootA] //all children of root A
							+ 1; 			// node A
		} else {
			changeParentNode(rootB, rootA);
			treeSize[rootA] = treeSize[rootA] + treeSize[rootB] + 1;
		}
				
	}
	
	/**
	 * Identify the root of a node, and use the opportunity to compress the tree 
	 * 
	 * Worst case: O(lg n)
	 * 
	 * @param node
	 * @return
	 */
	protected int findRoot(int node) {
		int root = node;
		while(!isRoot(root)) {//move up the tree
			root = getParentNodes()[root];
		}
		int moving;
		while(!isRoot(node)) {//compress the tree
			moving = node;
			node = getParentNodes()[node];// go to the parent
			getParentNodes()[moving] = root;// change parent of current node
			treeSize[node] = treeSize[node] - treeSize[moving];
		}
		return root;
	}


}
