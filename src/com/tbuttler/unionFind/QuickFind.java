package com.tbuttler.unionFind;

/**
 * Uses an array with set ids (as integers) to keep track for each element to which set it belongs.
 * @author Tanja
 *
 */
public class QuickFind implements UnionFind {
	
	/**
	 * For each element e in 0 to size, sets[e] identifies a set id for the element e.
	 */
	private int[] sets;
	
	public QuickFind(int size) {
		sets = new int[size];
		//we assume every element is disconnected
		for (int position = 0; position < sets.length; position++) {
			sets[position] = position;
		}
	}

	@Override
	public boolean connected(int a, int b) {
		return sets[a] == sets[b];
	}

	@Override
	public void union(int a, int b) {
		int setA = sets[a];
		int setB = sets[b];
		for (int position = 0; position < sets.length; position++) {
			if(sets[position] == setB)
				sets[position] = setA;
		}
	}

}
