package com.tbuttler.unionFind;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.tbuttler.unionFind.QuickFind;
import com.tbuttler.unionFind.QuickUnion;
import com.tbuttler.unionFind.QuickUnionWPathCompression;
import com.tbuttler.unionFind.UnionFind;
import com.tbuttler.unionFind.WeightedQuickUnion;

/**
 * Parameterized tests for the API of {@link UnionFind}. The tests run with various implementations of the API.  
 * @author Tanja
 *
 */
@RunWith(value = Parameterized.class)
public class TestUnionFind {
	
	private Class<? extends UnionFind> unionType;
	private UnionFind union;
	
	@Parameters
	public static Collection<Class<? extends UnionFind>> unionTypes() {
		ArrayList<Class<? extends UnionFind>> types = new ArrayList<>();
		types.add(QuickUnion.class);
		types.add(QuickFind.class);
		types.add(QuickUnionWPathCompression.class);
		types.add(WeightedQuickUnion.class);
		return types;
	}
	
	/**
	 * Constructor needed by framework to create a test class for a specific implementation of {@link UnionFind}.
	 * @param type
	 */
	public TestUnionFind(Class<? extends UnionFind>  type) {
		unionType = type;
	}
	
	/**
	 * Instantiate a new instance of a given implementation of {@link UnionFind}.
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	@Before
	public void initializeUnion() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//primitive types have a class
		union = unionType.getDeclaredConstructor(int.class).newInstance(20);
	}
	
	public UnionFind populate(UnionFind union) {
		union.union(5, 10);
		union.union(18, 3);
		union.union(6, 10);
		union.union(7,  8);
		union.union(9, 7);
		union.union(1, 2);
		union.union(2, 8);
		union.union(3, 4);
		return union;
	}
	
	/**
	 * If a single connection is entered, the union contains only that connection.
	 */
	@Test
	public void singleConnection() {
		union.union(14, 3);
		assertThat("connected nodes should be recognized as such", union.connected(3, 14), is(true));
		assertThat("unconnected nodes should be recognized as such", union.connected(8, 14), is(false));
		assertThat("previously queried unconnected nodes should be recognized as such", union.connected(5, 8), is(false));
	}
	
	/**
	 * If multiple connections are entered, the union contains these connections as well as indirect links
	 * formed through these connections.
	 */
	@Test
	public void multipleEntries() {
		union = populate(union);
		//should be connected
		assertThat("explicitly stated connection should be part of the union", union.connected(7, 9), is(true));
		assertThat("implicit connection should be part of the union", union.connected(6, 5), is(true));
		assertThat("last connection should be entered in implicit connections", union.connected(4, 18), is(true));
		//should not be connected
	}


}
