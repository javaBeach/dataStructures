package com.tbuttler.collection.queues;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

public class EmptyRandomizedQueue {
	
	private RandomizedQueue<Integer> queue;

	@Before
	public void setUp() throws Exception {
		queue = new RandomizedQueue<>();
	}

	@Test
	public void shouldBeEmpty() {
		assertThat("The queue should be empty.", queue.isEmpty(), is(true));
	}

	@Test
	public void shouldHaveSizeZero() {
		assertThat("An empty deque should have size 0", queue.size(), is(0));
	}

	@Test
	public void enqueShouldChangeState() {
		Integer value = new Integer(78);
		queue.enqueue(value);
		assertThat("After adding an item, the queue should not be empty", queue.isEmpty(), is(false));
		assertThat("After adding an item, the queue should have size 1", queue.size(), is(1));
	}

	@Test(expected = NoSuchElementException.class)
	public void dequeShouldThrowException() {
		queue.dequeue();
	}

	@Test(expected = NoSuchElementException.class)
	public void sampleShouldThrowException() {
		queue.sample();
	}
	
	@Test(expected = NoSuchElementException.class)
	public void iterNextShoudThrowException() {
		Iterator<Integer> iter = queue.iterator();
		iter.next();
	}

}
