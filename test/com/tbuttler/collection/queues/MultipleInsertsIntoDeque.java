package com.tbuttler.collection.queues;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import edu.princeton.cs.algs4.StdRandom;

public class MultipleInsertsIntoDeque {
	
	private Deque<Integer> deque;

	@Before
	public void setUp() throws Exception {
		deque = new Deque<>();
	}

	private Integer[] createContent(int size) {
		Integer[] content = new Integer[size];
		for (int i = 0; i < size; i++) {
			content[i] = StdRandom.uniform(size);
		}
		return content;
	}

	@Test
	public void addMultipleAtFront() {
		int count = 0;
		int finalSize = 17;
		Integer[] content = createContent(finalSize);
		for (Integer item : content) {
			deque.addFirst(item);
			count++;
			assertThat("After adding an item at the front, the deque should not be empty", deque.isEmpty(), is(false));
			assertThat("After adding an item at the front, the deque size should have increased", deque.size(), is(count));
		}
		assertThat("The number of iterations should have been equal to the final size of the deque",  iterElemCount(), is(finalSize));
	}
	
	@Test
	public void addMultipleAtEnd() {
		int count = 0;
		int finalSize = 105;
		Integer[] content = createContent(finalSize);
		for (Integer item : content) {
			deque.addLast(item);
			count++;
			assertThat("After adding an item at the end, the deque should not be empty", deque.isEmpty(), is(false));
			assertThat("After adding an item at the end, the deque size should have increased", deque.size(), is(count));
		}
		assertThat("The number of iterations should have been equal to the final size of the deque",  iterElemCount(), is(finalSize));
	}
	
	/**
	 * Randomly inserts items in the beginning or end of the queue.
	 */
	@Test
	public void mixedEnques() {
		int count = 0;
		int finalSize = 1009;
		Integer[] content = createContent(finalSize);
		for (Integer item : content) {
			addSomewhere(item);
			count++;
			assertThat("After adding an item at the end, the deque should not be empty", deque.isEmpty(), is(false));
			assertThat("After adding an item at the end, the deque size should have increased", deque.size(), is(count));
		}
		assertThat("The number of iterations should have been equal to the final size of the deque",  iterElemCount(), is(finalSize));
	}
	
	/**
	 * Randomly adds the item at the front or the end of the queue.
	 * @param item to be added to the queue
	 */
	private void addSomewhere(Integer item) {
		boolean isFront = StdRandom.bernoulli();
		if(isFront) {
			deque.addFirst(item);
		} else {
			deque.addLast(item);
		}
	}
	
	/**
	 * Uses a foreach-loop (which uses an Iterator) to move through the deque, and counts the loop cycles.
	 * 
	 * @return how often the block of the loop was executed (== how often {@link Iterator#next()} has been called.
	 */
	private int iterElemCount() {
		int count = 0;
		for (Integer integer : deque) {
			count++;
		}
		return count;
	}



}
