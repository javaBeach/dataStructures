package com.tbuttler.collection.queues;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.object.IsCompatibleType.typeCompatibleWith;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import edu.princeton.cs.algs4.StdRandom;

/**
 * Test cases for accessing (removing or sampling) elements from a
 * {@link RandomizedQueue}. The tests are parameterized, using queues of various
 * length larger than zero. The empty queue is tested in
 * {@link EmptyRandomizedQueue}.
 * 
 * @author Tanja
 *
 */
@RunWith(Parameterized.class)
public class AccessRandomizedQueue {

    /**
     * Queue instance to be tested. The queue is already populated with random
     * integers from the range [0, size).
     */
    private RandomizedQueue<Integer> queue;

    /**
     * Intended size of the queue.
     */
    private int size;

    /**
     * Defines the test parameters.
     * 
     * @return an array with various test sizes.
     */
    @Parameters
    public static Integer[] size() {
        return new Integer[] { 1, 2, 45, 64, 178 };
    }

    /**
     * Initializes a queue of size <code>size</code>.
     * 
     * @param size
     *            of the tested queue
     */
    public AccessRandomizedQueue(Integer size) {
        queue = createQueue(size);
        this.size = size;
    }

    /**
     * Checks the state of the sample item and the queue after a single sample.
     * In particular, the sample should be not null and have the expected type.
     * Also, the size of the queue should not change (i.e., the sample should
     * remain in the queue).
     */
    @Test
    public void sampleItems() {
        Integer sample = queue.sample();
        assertThat("The returned item should be of type Integer.",
                sample.getClass(), typeCompatibleWith(Integer.class));
        assertThat("Sampling should not remove items from the queue.",
                queue.size(), is(size));
    }

    /**
     * Checks the state of the item and the queue after a single dequeue. In
     * particular, the item should be not null and have the expected type. Also,
     * the size of the queue should be reduced by one (i.e., the item should
     * have been removed from the queue).
     */
    @Test
    public void dequeueItem() {
        Integer removed = queue.dequeue();
        assertThat("The returned item should be of type Integer.",
                removed.getClass(), typeCompatibleWith(Integer.class));
        assertThat("Dequeue should remove items from the queue.", queue.size(),
                is(size - 1));
    }

    /**
     * When sampling from the queue several times the values should, on average,
     * be different. This test draws 11 samples and checks whether, from the
     * last 10, some differ from the first one.
     * 
     * NOTE: there is a small probability that this test fails even though the
     * implementation of the queue is correct
     */
    @Test
    public void testNonEqualSampling() {
        int sameSampleValue = 0;
        int sampleOne = queue.sample().intValue();
        for (int i = 0; i < 10; i++) {
            sameSampleValue = (sampleOne == queue.sample().intValue())
                    ? sameSampleValue++ : sameSampleValue;
        }
        assertThat(sameSampleValue, is(not(10)));
    }

    /**
     * Creates a queue and fills it with a random selection of Integers without
     * running any tests.
     */
    private RandomizedQueue<Integer> createQueue(int size) {
        RandomizedQueue<Integer> queue = new RandomizedQueue<>();
        for (int i = 0; i < size; i++) {
            queue.enqueue(StdRandom.uniform(size));
        }
        return queue;
    }

}
