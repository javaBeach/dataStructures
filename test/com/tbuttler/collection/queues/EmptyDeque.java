package com.tbuttler.collection.queues;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests the default behaviour and corner cases for an empty deque (obtained by creating a new Deque instance).
 * 
 * @author Tanja
 *
 */
public class EmptyDeque {
	
	private Deque<Object> emptyDeque;
	
	@Before
	public void intitializeDeque() {
		emptyDeque = new Deque<>();
	}

	@Test
	public void shouldBeEmpty() {
		assertThat("The deque should be empty.", emptyDeque.isEmpty(), is(true));
	}

	@Test
	public void shouldHaveSizeZero() {
		assertThat("An empty deque should have size 0", emptyDeque.size(), is(0));
	}

	@Test
	public void addFirstChangingState() {
		emptyDeque.addFirst(new Object());
		assertThat("After adding an item at the front, the deque should not be empty", emptyDeque.isEmpty(), is(false));
		assertThat("After adding an item at the front, the deque should have size 1", emptyDeque.size(), is(1));
	}

	@Test
	public void addLastChangingState() {
		emptyDeque.addLast(new Object());
		assertThat("After adding an item at the end, the deque should not be empty", emptyDeque.isEmpty(), is(false));
		assertThat("After adding an item at the end, the deque should have size 1", emptyDeque.size(), is(1));
	}

	@Test(expected = NoSuchElementException.class)
	public void removeFirst() {
		emptyDeque.removeFirst();
	}

	@Test(expected = NoSuchElementException.class)
	public void testRemoveLast() {
		emptyDeque.removeLast();
	}
	
	@Test
	public void iteratorHasNext() {
		Iterator<Object> iter = emptyDeque.iterator();
		assertThat("An empty iterator should not have a next element", iter.hasNext(), is(false));
	}
	
	@Test(expected = NoSuchElementException.class)
	public void iteratorNext() {
		Iterator<Object> iter = emptyDeque.iterator();
		iter.next();
	}

}
