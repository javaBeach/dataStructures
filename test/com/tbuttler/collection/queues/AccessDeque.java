package com.tbuttler.collection.queues;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.princeton.cs.algs4.StdRandom;

public class AccessDeque {

	private Deque<Integer> deque;

	@Before
	public void setUp() throws Exception {
		deque = new Deque<>();
	}

	private void addContent(int numberItems) {
		for (int i = 0; i < numberItems; i++) {
			deque.addFirst(StdRandom.uniform(numberItems));
		}
	}

	@Test
	public void removeEnd() {
		int numberItems = 345;
		addContent(numberItems);
		while (!deque.isEmpty()) {
			deque.removeLast();
			numberItems--;
			assertThat("The number of items should decrease by one when one item is removed from the end.",
					deque.size(), is(numberItems));
		}
		assertThat("The emptied deque should have size 0.", deque.size(), is(0));
	}
	
	@Test
	public void removeFront() {
		int numberItems = 132;
		addContent(numberItems);
		while (!deque.isEmpty()) {
			deque.removeFirst();
			numberItems--;
			assertThat("The number of items should decrease by one when one item is removed from the front.",
					deque.size(), is(numberItems));
		}
		assertThat("The emptied deque should have size 0.", deque.size(), is(0));
	}
	
	@Test
	public void mixedRemovals() {
		int numberItems = 492;
		addContent(numberItems);
		while(!deque.isEmpty()) {
			removeSomewhere();
			numberItems--;
			assertThat("After removing an item, the deque size should have decreased", deque.size(), is(numberItems));
		}
		assertThat("After removing the last item, the deque should be empty", deque.isEmpty(), is(true));
	}

	/**
	 * Randomly adds the item at the front or the end of the queue.
	 */
	private void removeSomewhere() {
		boolean isFront = StdRandom.bernoulli();
		if(isFront) {
			deque.removeFirst();
		} else {
			deque.removeLast();
		}
	}
	


}
