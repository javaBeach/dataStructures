package com.tbuttler.collection.queues;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.object.IsCompatibleType.typeCompatibleWith;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import edu.princeton.cs.algs4.StdRandom;

/**
 * Test case for adding (enqueue) multiple random Integers to a {@link RandomizedQueue}.
 * The tests are parameterized, using queues of various
 * final length larger than zero. The empty queue is tested in
 * {@link EmptyRandomizedQueue}.
 * 
 * @author Tanja
 *
 */
@RunWith(Parameterized.class)
public class MultipleInsertsIntoRandomizedQueue {

    /**
     * The queue to be tested. Should be empty at the beginning of the test, and should have
     * the parameterized size at the end of the test.
     */
    private RandomizedQueue<Integer> queue;
    
    /**
     * An array of the length size, filled with random Integers from [0, size).
     */
    private Integer[] content;
    
    /**
     * Provides the final sizes of the queues.
     * 
     * @return array with sizes of queues, each item larger than zero
     */
    @Parameters
    public static Integer[] size() {
        return new Integer[] { 1, 2, 165, 64, 78 };
    }

    /**
     * Create a test case for adding items to a queue.
     * 
     * @param size of the queue at the end of the test
     */
    public MultipleInsertsIntoRandomizedQueue(Integer size) {
        queue = new RandomizedQueue<>();
        content = createContent(size);
    }
    
    /**
     * Helper method to create <code>size</code> randomized Integers.
     * 
     * @param size number of integer to create
     * @return array with the integers
     */
    private Integer[] createContent(int size) {
        Integer[] content = new Integer[size];
        for (int i = 0; i < size; i++) {
            content[i] = StdRandom.uniform(size);
        }
        return content;
    }

    /**
     * Inserts multiple entries into the queue, and checks that the state is update with the number of elements.
     */
    @Test
    public void addMultipleEntries() {
        int count = 0;
        for (int i = 0; i < content.length; i++) {
            queue.enqueue(content[i]);
            count++;
            assertThat("After adding an item, the queue should not be empty",
                    queue.isEmpty(), is(false));
            assertThat(
                    "After adding an item, the queue size should have increased",
                    queue.size(), is(count));
        }
    }
    
    /**
     * Inserts multiple entries and checks whether the iterator moves over all elements in the queue.
     */
    @Test
    public void iteratingOverMultipleEntries() {
        int finalSize = content.length;
        fillQueue();
        int count = 0;
        for (Integer integer : queue) {
            count++;
            assertThat(integer.getClass(), typeCompatibleWith(Integer.class));
        }
        assertThat(
                "The number of iterations should have been equal to the size of the queue.",
                count, is(finalSize));
    }
    
    /**
     * Fills the queue with the content without running any tests.
     */
    private void fillQueue() {
        for (int i = 0; i < content.length; i++) {
            queue.enqueue(content[i]);
        }
    }
    

    /**
     * Corner case: if a null item is inserted, the queue should throw a
     * NullPointerException.
     */
    @Test(expected = NullPointerException.class)
    public void nullEnques() {
        fillQueue();
        queue.enqueue(null);
    }

}
