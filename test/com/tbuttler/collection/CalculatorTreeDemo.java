package com.tbuttler.collection;

import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.tbuttler.collection.CalculatorTree.Operation;
import com.tbuttler.collection.CalculatorTree.Value;

/**
 * Testing the calculator tree.
 * 
 * @author Tanja
 *
 */
public class CalculatorTreeDemo {
	
	/**
	 * A single binary operation
	 */
	@Test
	public void onePlusTwo() {
		CalculatorTree tree = new CalculatorTree();
		Value one = new Value(1);
		Value two = new Value(2);
		Operation op = new Operation(Operation.PLUS);
		tree.startWith(one, op, two);
		assertThat("1 + 2 = 3", tree.calc(), closeTo(3, 0.01));
		tree.append(two, op);
		assertThat("1 + 2 + 2 = 5", tree.calc(), closeTo(5, 0.01));
	}
	
	/**
	 * Emulating input from a GUI with a text field and operation buttons: the values are entered
	 * together with the next operation (e.g., (one, plus)).
	 */
	@Test
	public void guiInput() {
		CalculatorTree tree = new CalculatorTree();
		Value one = new Value(1);
		Value two = new Value(2);
		Operation op = new Operation(Operation.PLUS);
		tree.startWith(one, op);
		tree.closeWith(two);
		assertThat("1 + 2 = 3", tree.calc(), closeTo(3, 0.01));
		tree.append(op, two);
		assertThat("1 + 2 + 2 = 5", tree.calc(), closeTo(5, 0.01));
	}
	
	/**
	 * 2 * (2 + 3)
	 * putting the calculation inside brackets into a sub-tree
	 */
	@Test
	public void brackets() {
		CalculatorTree left = new CalculatorTree();//left of an open bracket
		CalculatorTree inside = new CalculatorTree();
		Value three = new Value(3);
		Value two = new Value(2);
		Operation plus = new Operation(Operation.PLUS);
		Operation mult = new Operation(Operation.MULT);
		
		inside.startWith(two, plus, three);
		left.append(two, mult);
		left.inBrackets(inside);
		
		assertThat("2 *(2 + 3) = 10", left.calc(), closeTo(10, 0.01));
	}
	
}
