package com.tbuttler.collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Iterator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class LeftRightAPI {
	
	public class Fixture {
		
		String e1 = "object 1";
		String e2 = "object 2";
		String e3 = "object 3";

		public Fixture(LeftRightList<String> list) {
			list.add(e1);
			list.add(e2);
			list.add(e3);
		}
	}

	private Fixture data;
	private LeftRightList<String> list;

	@Before
	public void setUp() {
		list = new LeftRightList<String>();
	}

	@Test
	public void create() {
		assertThat("The list should not contain any elements",list.size(), is(0));
		assertTrue("The list should be empty", list.isEmpty());
	}
	
	@Test
	public void addOneElement() {
		String e = "hello world";
		list.add(e);
		assertThat("The list should have one element.", list.size(), is(1));
		assertFalse("The list should not be empty", list.isEmpty());
		assertThat("The first object should be my e", list.insertionOrderIterator().next(), is(e));
	}
	
	@Test
	public void addTwoElements() {
		String e1 = "object 1";
		String e2 = "object 2";
		list.add(e1);
		list.add(e2);
		Iterator<String> iter = list.insertionOrderIterator();
		assertThat("The list should have two elements.", list.size(), is(2));
		assertThat("The string representation orders the elements in their internal order", list.toString(), is("[object 1, object 2]"));
		assertThat("The first element should be the first object", iter.next(), is(e1));
		assertThat("The second element should be the second object", iter.next(), is(e2));
	}
	
	@Test
	public void stringRepresentation() {
		data = new Fixture(list);
		assertThat("The string representation orders the elements in their internal order", list.toString(), is("[object 3, object 1, object 2]"));
	}
	
	@Test
	public void iterateThroughElements() {
		data = new Fixture(list);
		Iterator<String>  iter = list.insertionOrderIterator();
		assertThat("The first element should be the first object", iter.next(), is(data.e1));
		assertThat("The iterator has more elements", iter.hasNext(), is(true));
		assertThat("The second element should be the second object", iter.next(), is(data.e2));
		assertThat("The iterator has more elements", iter.hasNext(), is(true));
		assertThat("The last element should be the last object",  iter.next(), is(data.e3));
		assertThat("The iterator has no more elements", iter.hasNext(), is(false));
	}
}
